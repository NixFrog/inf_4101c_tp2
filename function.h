/*******************************************************************/ 
/*  INF-4101C - Travaux pratiques 2
 *  Optimisation d'implantation du filtre de Sobel
 *  Eva Dokladalova, september 2015
 */

#ifndef FUNCTION_H_ 
#define FUNCTION_H_

#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdint.h>
#include <ctype.h>
#include <math.h>

// types definitions
typedef unsigned char uint8;

// buffer size for input image reading
#define BSIZE 1000

// function declarations
uint8 * readimg(char *, int *, int *, int *);
void writeimg(char *, uint8 *, int , int, int);

void  sobel(uint8 *im_in, uint8 *im_out, int rw , int cl);
int sobelMatrixApplication(int i00, int i01, int i02, int i10, int i12, int i20, int i21, int i22);

void sobel_loops_unroll_2(uint8 *, uint8 *, int, int);
void sobel_loops_unroll_4(uint8 *, uint8 *, int, int);
void sobel_loops_unroll_8(uint8 *, uint8 *, int, int);

void sobel_opti(uint8 *im_in, uint8 *im_out, int rw , int cl);
int sobelMatrixApplication_opti(int i00, int i01, int i02, int i10, int i12, int i20, int i21, int i22);

void sobel_opti_better(uint8 *im_in, uint8 *im_out, int rw , int cl);
void sobel_opti_ulti(uint8 *im_in, uint8 *im_out, int rw , int cl);

void sobel__doubleLoop_opti(uint8 *im_in, uint8 *im_out, int rw , int cl);
void sobel__doubleLoop_opti_2(uint8 *im_in, uint8 *im_out, int rw , int cl);
void sobel__doubleLoop_opti_unroll_8(uint8 *im_in, uint8 *im_out, int rw , int cl);


#endif



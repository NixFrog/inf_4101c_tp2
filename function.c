 	/*******************************************************************/ 
/*  INF-4101C - Travaux pratiques 2
 *  Optimisation d'implantation du filtre de Sobel
 *  Eva Dokladalova, september 2015
 */

#include "function.h"

//------------------------------------------------------------------
// ONLY FOR PGN IMAGES type P2 !!!
//------------------------------------------------------------------
//open image file and access data, allocate an image with file data; 
 uint8 * readimg(char *filename, int *rs, int *cs, int *vs)
 {
	uint8 *im; // input data
	char *rd;
	int c,v;
	int rw; //row size
	int cl; //column size
	char buffer[BSIZE];
	FILE *fd = NULL;
	
	// open input file 
	fd = fopen(filename,"r");
	if (fd==NULL){
		fprintf(stderr, "Input image reading failed)\n");
		return (0);
	}
	
	// verify file type BLACK ADN WHITE ASCII PGN = P2
	rd = fgets(buffer, BSIZE, fd);
	if (!rd){
		fprintf(stderr, "Reading failed (%s)\n", filename);
		return 0;
	}
	if ((buffer[0] != 'P') || (buffer[1]!= '2')){   
		fprintf(stderr,"%s : invalid image format\n", filename);
		return (0);
	}
	do{ // comment in pgn file ?
		rd = fgets(buffer, BSIZE, fd); /* commentaire #*/
		if (!rd){
			fprintf(stderr, "%s: fgets returned without reading\n", filename);
			return 0;
		}		
	} while (!isdigit(buffer[0]));

	// scan image size rw = number of rows; cl = number of columns
	c = sscanf(buffer, "%d %d", &rw, &cl);
	rd = fgets(buffer, BSIZE, fd);
	// scan maximal pixel value, should be == 255
	c = sscanf(buffer, "%d", &v);
	//data allocation and initialization
	im = (uint8 *) calloc (rw*cl,sizeof(uint8));
	if (im==NULL){
		fprintf(stderr, "Data allocation (failed %d) bytes)\n", rw*cl);
		return (0);
	}
	int i;
	// data initialization
	int N = rw*cl*sizeof(uint8);
	for (i=0;i<N;i++){
		fscanf(fd, "%d", &c);
		im[i]=(uint8) c;
	}
	// close file and return pointer to the iamge data
	fclose(fd);  
	*cs = cl;
	*rs = rw;
	*vs = v;
	return(im);
}

//------------------------------------------------------------------
// ONLY FOR PGN IMAGES type P2 !!!
//------------------------------------------------------------------
// write image in a file
//----------------------------------------------------------------
void writeimg(char *filename, uint8 *im, int rs, int cs, int vs)
{
	int cl;
	int rw;
	int v;
	int N;
	FILE *fd_out;
	int i;

	cl = cs;
	rw = rs;
	v = vs;
	N = cl*rw;

	fd_out = fopen(filename,"w");
	if (!fd_out)
	{
		fprintf(stderr, "%s: cannot open file\n", filename);
		exit(0);
	} 
	fputs("P2\n", fd_out);
	fprintf(fd_out, "# comment\n");
	fprintf(fd_out, "%d %d\n", rw,cl);
	fprintf(fd_out, "%d\n", v);
	for (i = 0; i < N; i++)
	{ 
		fprintf(fd_out, "%d \n", im[i]);
	}      
	fclose(fd_out);
}


//--------------------------------------------------------------
// image processing
//-------------------------------------------------------------

void  sobel(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int i=0;
	for (i = 0 ; i < rw*(cl-2) - 2; i++) {
		if ((i+1) % rw != 0 && (i+2) % rw != 0) {
			im_out[i+1] = sobelMatrixApplication(
				im_in[i],			im_in[i+1], 			im_in[i+2], 
				im_in[i + rw], 		/*   x   */				im_in[i + rw + 2],
				im_in[i + 2*rw], 	im_in[i + 2*rw + 1], 	im_in[i + 2*rw + 2]
			);
		}
	}
}

int sobelMatrixApplication(int i00, int i01, int i02, int i10, int i12, int i20, int i21, int i22){
	double H = -i00 + i02 - 2*i10 + 2*i12 - i20 + i22;
	double V = i00 + 2*i01 + i02 - i20 - 2*i21 - i22;
	int GN = (int)sqrt(H*H + V*V);

	// Check overflow
	if (GN > 255) {
		GN = 255;
	}
	
	return GN;
}


void sobel_loops_unroll_2(uint8 *im_in, uint8 *im_out, int rw , int cl){
	for (int i = 0 ; i < rw*(cl-2) -2; i+=2) {
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			im_out[i + 1] = sobelMatrixApplication(
				im_in[i],			im_in[i + 1], 			im_in[i + 2], 
				im_in[i + rw], 		/*   x   */				im_in[i + rw + 2],
				im_in[i + 2*rw], 	im_in[i + 2*rw + 1], 	im_in[i + 2*rw + 2]
			);
		}
		
		if ((i+2) % rw != 0 && (i+3) % rw != 0) { // exclude left and right borders
			im_out[i + 2] = sobelMatrixApplication(
				im_in[i + 1], 			im_in[i + 2], 			im_in[i + 3], 
				im_in[i + rw + 1], 		/*   x   */				im_in[i + rw + 3],
				im_in[i + 2*rw + 1],	im_in[i + 2*rw + 2], 	im_in[i + 2*rw + 3]
			);
		}
	}
}

void sobel_loops_unroll_4(uint8 *im_in, uint8 *im_out, int rw , int cl){
	for (int i = 0 ; i < rw*(cl-2) -2; i+=4) {
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			im_out[i + 1] = sobelMatrixApplication(
				im_in[i],			im_in[i + 1], 			im_in[i + 2], 
				im_in[i + rw], 		/*   x   */				im_in[i + rw + 2],
				im_in[i + 2*rw], 	im_in[i + 2*rw + 1], 	im_in[i + 2*rw + 2]
			);
		}
		
		if ((i+2) % rw != 0 && (i+3) % rw != 0) { // exclude left and right borders
			im_out[i + 2] = sobelMatrixApplication(
				im_in[i + 1], 			im_in[i + 2], 			im_in[i + 3], 
				im_in[i + rw + 1], 		/*   x   */				im_in[i + rw + 3],
				im_in[i + 2*rw + 1],	im_in[i + 2*rw + 2], 	im_in[i + 2*rw + 3]
			);
		}
		
		if ((i+3) % rw != 0 && (i+4) % rw != 0) { // exclude left and right borders
			im_out[i + 3] = sobelMatrixApplication(
				im_in[i + 2], 			im_in[i + 3], 			im_in[i + 4], 
				im_in[i + rw + 2], 		/*   x   */				im_in[i + rw + 4],
				im_in[i + 2*rw + 2],	im_in[i + 2*rw + 3], 	im_in[i + 2*rw + 4]
			);
		}

		if ((i+4) % rw != 0 && (i+5) % rw != 0) { // exclude left and right borders
			im_out[i + 4] = sobelMatrixApplication(
				im_in[i + 3], 			im_in[i + 4], 			im_in[i + 5], 
				im_in[i + rw + 3], 		/*   x   */				im_in[i + rw + 5],
				im_in[i + 2*rw + 3],	im_in[i + 2*rw + 4], 	im_in[i + 2*rw + 5]
			);
		}
	}
}

void sobel_loops_unroll_8(uint8 *im_in, uint8 *im_out, int rw , int cl){
	for (int i = 0 ; i < rw*(cl-2) -2; i+=8) {
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			im_out[i + 1] = sobelMatrixApplication(
				im_in[i],			im_in[i + 1], 			im_in[i + 2], 
				im_in[i + rw], 		/*   x   */				im_in[i + rw + 2],
				im_in[i + 2*rw], 	im_in[i + 2*rw + 1], 	im_in[i + 2*rw + 2]
			);
		}
		
		if ((i+2) % rw != 0 && (i+3) % rw != 0) { // exclude left and right borders
			im_out[i + 2] = sobelMatrixApplication(
				im_in[i + 1], 			im_in[i + 2], 			im_in[i + 3], 
				im_in[i + rw + 1], 		/*   x   */				im_in[i + rw + 3],
				im_in[i + 2*rw + 1],	im_in[i + 2*rw + 2], 	im_in[i + 2*rw + 3]
			);
		}
		
		if ((i+3) % rw != 0 && (i+4) % rw != 0) { // exclude left and right borders
			im_out[i + 3] = sobelMatrixApplication(
				im_in[i + 2], 			im_in[i + 3], 			im_in[i + 4], 
				im_in[i + rw + 2], 		/*   x   */				im_in[i + rw + 4],
				im_in[i + 2*rw + 2],	im_in[i + 2*rw + 3], 	im_in[i + 2*rw + 4]
			);
		}

		if ((i+4) % rw != 0 && (i+5) % rw != 0) { // exclude left and right borders
			im_out[i + 4] = sobelMatrixApplication(
				im_in[i + 3], 			im_in[i + 4], 			im_in[i + 5], 
				im_in[i + rw + 3], 		/*   x   */				im_in[i + rw + 5],
				im_in[i + 2*rw + 3],	im_in[i + 2*rw + 4], 	im_in[i + 2*rw + 5]
			);
		}

		if ((i+5) % rw != 0 && (i+6) % rw != 0) { // exclude left and right borders
			im_out[i + 5] = sobelMatrixApplication(
				im_in[i + 4], 			im_in[i + 5], 			im_in[i + 6], 
				im_in[i + rw + 4], 		/*   x   */				im_in[i + rw + 6],
				im_in[i + 2*rw + 4],	im_in[i + 2*rw + 5], 	im_in[i + 2*rw + 6]
			);
		}

		if ((i+6) % rw != 0 && (i+7) % rw != 0) { // exclude left and right borders
			im_out[i + 6] = sobelMatrixApplication(
				im_in[i + 5], 			im_in[i + 6], 			im_in[i + 7], 
				im_in[i + rw + 5], 		/*   x   */				im_in[i + rw + 7],
				im_in[i + 2*rw + 5],	im_in[i + 2*rw + 6], 	im_in[i + 2*rw + 7]
			);
		}

		if ((i+7) % rw != 0 && (i+8) % rw != 0) { // exclude left and right borders
			im_out[i + 7] = sobelMatrixApplication(
				im_in[i + 6], 			im_in[i + 7], 			im_in[i + 8], 
				im_in[i + rw + 6], 		/*   x   */				im_in[i + rw + 8],
				im_in[i + 2*rw + 6],	im_in[i + 2*rw + 7], 	im_in[i + 2*rw + 8]
			);
		}


		if ((i+8) % rw != 0 && (i+9) % rw != 0) { // exclude left and right borders
			im_out[i + 8] = sobelMatrixApplication(
				im_in[i + 7], 			im_in[i + 8], 			im_in[i + 9], 
				im_in[i + rw + 7], 		/*   x   */				im_in[i + rw + 9],
				im_in[i + 2*rw + 7],	im_in[i + 2*rw + 8], 	im_in[i + 2*rw + 9]
			);
		}
	}
}


int sobelMatrixApplication_opti(int i00, int i01, int i02, int i10, int i12, int i20, int i21, int i22){
	int isave = i02 - i20;
	int H = -i00 + isave - (i10<<1) + (i12<<1) + i22;
	int V = i00 + (i01<<1) + isave - (i21<<1) - i22;
	return (abs(H) + abs(V) < 255 ? abs(H) + abs(V) : 255);
}

void sobel_opti(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int max = rw*(cl-2) -2;
	for (int i = 0 ; i < max; i++) {
		int rw_i_sum = rw+i;
		int rw_double_i_sum = (rw<<1) +i;
		int i_sum_2 = i+2;
		int rw_double_i_sum_one = rw_double_i_sum +1;
		int rw_double_i_sum_two = rw_double_i_sum +2;
		
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			im_out[i + 1] = sobelMatrixApplication(
				im_in[i],					im_in[i + 1], 					im_in[i_sum_2], 
				im_in[rw_i_sum], 			/*   x   */						im_in[rw_i_sum + 2],
				im_in[rw_double_i_sum], 	im_in[rw_double_i_sum_one], 	im_in[rw_double_i_sum_two]
			);
		}
		i++;
		
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			im_out[i + 1] = sobelMatrixApplication(
				im_in[i],					im_in[i + 1], 					im_in[i_sum_2], 
				im_in[rw_i_sum], 			/*   x   */						im_in[rw_i_sum + 2],
				im_in[rw_double_i_sum], 	im_in[rw_double_i_sum_one], 	im_in[rw_double_i_sum_two]
			);
		}
	}
}

void sobel_opti_better(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int max = rw*(cl-2) -2;
	uint8 *im_in0 = &im_in[0];
	uint8 *im_out0 = &im_out[0];

	for (int i = 0 ; i < max; i+=2) {
		int rw_double = rw<<1;
		int rw_double_sum_one = rw_double +1;
		int rw_sum_two = rw + 2;
		int rw_double_sum_two = rw_double +2;

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
	}
}

void sobel_opti_ulti(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int max = rw*(cl-2) -2;
	uint8 *im_in0 = &im_in[0];
	uint8 *im_out0 = &im_out[0];

	for (int i = 0 ; i < max; i+=8) {
		int rw_double = rw<<1;
		int rw_double_sum_one = rw_double +1;
		int rw_sum_two = rw + 2;
		int rw_double_sum_two = rw_double +2;

		im_out0++;
		if ((i+1) % rw != 0 && (i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
		
		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;

		im_out0++;
		if ((i+2) % rw != 0) { // exclude left and right borders
			*(im_out0) = sobelMatrixApplication(
				*(im_in0),					*(im_in0 +1), 					*(im_in0+2), 
				*(im_in0 + rw), 			/*   x   */						*(im_in0 + rw_sum_two),
				*(im_in0 + rw_double), 		*(im_in0 + rw_double_sum_one), 	*(im_in0 + rw_double_sum_two)
			);
		}
		im_in0++;
	}
}


void sobel__doubleLoop_opti(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int H, V, root;
	for(int i = 0; i < rw-2; i++){
		for(int j = 0; j < cl-2	; j++){
			int i1 = i+1;
			int i2 = i+2;
			int j1 = j+1;
			int j2 = j+2;
			int jrw = j*rw;
			int j2rw = j2*rw;
			int j1rw = j1*rw;

			H = - im_in[i+jrw]
				+ im_in[i+j2rw]
				- (im_in[i1+jrw]<<1)
				+ (im_in[i1+j2rw]<<1)
				- im_in[i2+jrw]
				+ im_in[i2+j2rw];

			V = im_in[i+jrw]
				+ (im_in[i+j1rw]<<1)
				+ im_in[i+j2rw]
				- im_in[i2+jrw]
				- (im_in[i2+j1rw]<<1)
				- im_in[i2+j2rw];
			
			root = abs(H) + abs(V);
			im_out[i1+j1*rw] = ( root < 255 ? root : 255);
		}
	}
}

void sobel__doubleLoop_opti_2(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int H, V, root;
	for(int i = 0; i < rw-2; i++){
		for(int j = 0; j < cl-2	; j++){
			int i1 = i+1;
			int i2 = i+2;
			int j1 = j+1;
			int j2 = j+2;
			int rw_j_sum_i = i+j*rw;
			int rw_j2_sum_i = i+j2*rw;
			int rw_j_sum_i1 = i1+j*rw;
			int rw_j2_sum_i1 = i1+j2*rw;
			int rw_j_sum_i2 = i2+j*rw;
			int rw_j2_sum_i2 = i2+j2*rw;
			int rw_j1_sum_i = i+j1*rw;
			int rw_j1_sum_i2 = i2+j1*rw;


			H = - im_in[rw_j_sum_i]
				+ im_in[rw_j2_sum_i]
				- 2*im_in[rw_j_sum_i1]
				+ 2*im_in[rw_j2_sum_i1]
				- im_in[rw_j_sum_i2]
				+ im_in[rw_j2_sum_i2];

			V = im_in[rw_j_sum_i]
				+ 2 * im_in[rw_j1_sum_i]
				+ im_in[rw_j2_sum_i]
				- im_in[rw_j_sum_i2]
				- 2 * im_in[rw_j1_sum_i2]
				- im_in[rw_j2_sum_i2];
			
			root = abs(H) + abs(V);
			im_out[i1+j1*rw] = ( root < 255 ? root : 255);
		}
	}
}

void sobel__doubleLoop_opti_unroll_8(uint8 *im_in, uint8 *im_out, int rw , int cl){
	int H, V, root;
	for(int i = 0; i < rw-2; i+=8){
		for(int j = 0; j < cl-2	; j++){
			int i1 = i+1;
			int i2 = i+2;
			int j1 = j+1;
			int j2 = j+2;

			H = - im_in[i+j*rw]
				+ im_in[i+j2*rw]
				- 2*im_in[i1+j*rw]
				+ 2*im_in[i1+j2*rw]
				- im_in[i2+j*rw]
				+ im_in[i2+j2*rw];

			V = im_in[i+j*rw]
				+ 2 * im_in[i+j1*rw]
				+ im_in[i+j2*rw]
				- im_in[i2+j*rw]
				- 2 * im_in[i2+j1*rw]
				- im_in[i2+j2*rw];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+2;
			int i2 = i+3;
			int j1 = j+2;
			int j2 = j+3;

			H = - im_in[i+j*rw+1]
				+ im_in[i+j2*rw+1]
				- 2*im_in[i1+j*rw+1]
				+ 2*im_in[i1+j2*rw+1]
				- im_in[i2+j*rw+1]
				+ im_in[i2+j2*rw+1];

			V = im_in[i+j*rw]
				+ 2 * im_in[i+j1*rw+1]
				+ im_in[i+j2*rw+1]
				- im_in[i2+j*rw+1]
				- 2 * im_in[i2+j1*rw+1]
				- im_in[i2+j2*rw+1];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+1] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+3;
			int i2 = i+4;
			int j1 = j+3;
			int j2 = j+4;

			H = - im_in[i+j*rw+2]
				+ im_in[i+j2*rw+2]
				- 2*im_in[i1+j*rw+2]
				+ 2*im_in[i1+j2*rw+2]
				- im_in[i2+j*rw+2]
				+ im_in[i2+j2*rw+2];

			V = im_in[i+j*rw+2]
				+ 2 * im_in[i+j1*rw+2]
				+ im_in[i+j2*rw+2]
				- im_in[i2+j*rw+2]
				- 2 * im_in[i2+j1*rw+2]
				- im_in[i2+j2*rw+2];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+2] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+4;
			int i2 = i+5;
			int j1 = j+4;
			int j2 = j+5;

			H = - im_in[i+j*rw+3]
				+ im_in[i+j2*rw+3]
				- 2*im_in[i1+j*rw+3]
				+ 2*im_in[i1+j2*rw+3]
				- im_in[i2+j*rw+3]
				+ im_in[i2+j2*rw+3];

			V = im_in[i+j*rw+3]
				+ 2 * im_in[i+j1*rw+3]
				+ im_in[i+j2*rw+3]
				- im_in[i2+j*rw+3]
				- 2 * im_in[i2+j1*rw+3]
				- im_in[i2+j2*rw+3];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+3] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+5;
			int i2 = i+6;
			int j1 = j+5;
			int j2 = j+6;

			H = - im_in[i+j*rw+4]
				+ im_in[i+j2*rw+4]
				- 2*im_in[i1+j*rw+4]
				+ 2*im_in[i1+j2*rw+4]
				- im_in[i2+j*rw+4]
				+ im_in[i2+j2*rw+4];

			V = im_in[i+j*rw+4]
				+ 2 * im_in[i+j1*rw+4]
				+ im_in[i+j2*rw+4]
				- im_in[i2+j*rw+4]
				- 2 * im_in[i2+j1*rw+4]
				- im_in[i2+j2*rw+4];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+4] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+6;
			int i2 = i+7;
			int j1 = j+6;
			int j2 = j+7;

			H = - im_in[i+j*rw+5]
				+ im_in[i+j2*rw+5]
				- 2*im_in[i1+j*rw+5]
				+ 2*im_in[i1+j2*rw+5]
				- im_in[i2+j*rw+5]
				+ im_in[i2+j2*rw+5];

			V = im_in[i+j*rw+5]
				+ 2 * im_in[i+j1*rw+5]
				+ im_in[i+j2*rw+5]
				- im_in[i2+j*rw+5]
				- 2 * im_in[i2+j1*rw+5]
				- im_in[i2+j2*rw+5];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+5] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+7;
			int i2 = i+8;
			int j1 = j+7;
			int j2 = j+8;

			H = - im_in[i+j*rw+6]
				+ im_in[i+j2*rw+6]
				- 2*im_in[i1+j*rw+6]
				+ 2*im_in[i1+j2*rw+6]
				- im_in[i2+j*rw+6]
				+ im_in[i2+j2*rw+6];

			V = im_in[i+j*rw+6]
				+ 2 * im_in[i+j1*rw+6]
				+ im_in[i+j2*rw+6]
				- im_in[i2+j*rw+6]
				- 2 * im_in[i2+j1*rw+6]
				- im_in[i2+j2*rw+6];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+6] = ( root < 255 ? root : 255);
		}

		for(int j = 0; j < cl-2	; j++){
			int i1 = i+8;
			int i2 = i+9;
			int j1 = j+8;
			int j2 = j+9;

			H = - im_in[i+j*rw+7]
				+ im_in[i+j2*rw+7]
				- 2*im_in[i1+j*rw+7]
				+ 2*im_in[i1+j2*rw+7]
				- im_in[i2+j*rw+7]
				+ im_in[i2+j2*rw+7];

			V = im_in[i+j*rw+7]
				+ 2 * im_in[i+j1*rw+7]
				+ im_in[i+j2*rw+7]
				- im_in[i2+j*rw+7]
				- 2 * im_in[i2+j1*rw+7]
				- im_in[i2+j2*rw+7];
			

		
			root = abs(H) + abs(V);
			im_out[i1+j1*rw+7] = ( root < 255 ? root : 255);
		}
	}
}


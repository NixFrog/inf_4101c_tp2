/*******************************************************************/ 
/*  INF-4101C - Travaux pratiques 2
 *  Optimisation d'implantation du filtre de Sobel
 *  Eva Dokladalova, september 2015
 */
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <stdint.h>

#include "function.h"

//-----------------------------------------------------------
// MAIN FUNCTION
//-----------------------------------------------------------
int main (int argc, char *argv[])
{
  if(argc != 3){
    printf("Incorrect usage: prog <filepath_in> <filepath_out>\n");
    return 1;
  }
  //varaible declaration
  char *filename=argv[1];
  char *file_out=argv[2];

  uint8 *im;
  uint8 *im_out;
  
  int v;  // max pixel value
  int rw; // row size
  int cl; // column size
  
  

  //-------------------------------------------------------------
  // OPEN IMAGE DATA AND ALLOCATE INPUT IMAGE MEMORY
  //-------------------------------------------------------------
  im = readimg(filename, &rw, &cl, &v);
 
  //-------------------------------------------------------------
  // image processing
  // - don't forget output data allocation if necessary
  // - put sobel function here
  //-------------------------------------------------------------
	im_out = (uint8 *) calloc (rw*cl,sizeof(uint8));
	for(int i=0; i<100; i++){
	   sobel(im,im_out,rw,cl);
	   sobel_loops_unroll_2(im,im_out,rw,cl);
	   sobel_loops_unroll_4(im,im_out,rw,cl);
	   sobel_loops_unroll_8(im,im_out,rw,cl);
	   sobel_opti(im,im_out,rw,cl);
	   sobel_opti_better(im,im_out,rw,cl);
	   sobel_opti_ulti(im,im_out,rw,cl);
	   sobel__doubleLoop_opti(im,im_out,rw,cl);
	   sobel__doubleLoop_opti_2(im,im_out,rw,cl);
	   sobel__doubleLoop_opti_unroll_8(im,im_out,rw,cl);
  	}
  
  //-------------------------------------------------------------
  // WRITE RESULT IN A PGN IMAGE 
  //-------------------------------------------------------------
  writeimg(file_out, im_out, rw, cl, v);

  return(0);
}


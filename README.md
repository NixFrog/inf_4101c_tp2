# README #

## Optimization of a Sobel filter in C ##
This is a basic optimization of a Sobel filter, using only simple operations (no multithreading). We propose several implementations of this filter, as it was realized as an exercise for school.